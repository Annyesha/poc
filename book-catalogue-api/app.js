const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser'); // body-parser module parses the JSON and exposes it on req. body
const cors = require('cors'); // Cross-Origin Resource Sharing allow or restrict requested resources on a web server
const app = express()
const port = 3000;

//this line is required to parse the request body	
app.use(express.json())

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* Read - GET method */
app.get('/books', (req, res) => {
    const books = getData()
    res.send(books)
})

/* Read book details for a particular isbn - GET method */
app.get('/books/:isbn', (req, res) => {
    // reading isbn from the URL
    const isbn = req.params.isbn;
    const books = getData()
    // searching books for the isbn
    for (let book of books) {
        if (book.isbn === isbn) {
            res.json(book);
            return;
        }
    }
    // sending 404 when book is not found
    res.status(404).send('Book not found');
});

/* Display book details for a particular search expression - GET method */
app.get('/books/search/:expression', (req, res) => {
    // reading search expression from the URL
	var expression = req.params.expression;
    const books = getData();
	var filteredBooks = "[";
	var count = 0;
    // searching books for the search expression
    for (let book of books) {
		if (book.isbn.search(expression) != -1 || book.title.search(expression) != -1 || book.author.search(expression) != -1)	{
			if(count != 0){
				filteredBooks =   filteredBooks + ','; 
            }
            filteredBooks = filteredBooks + JSON.stringify(book) ;
			count = count + 1;	
        }
    }
	filteredBooks = filteredBooks + ']';
	var jsonfilteredBooks = JSON.parse(filteredBooks);
	res.json(jsonfilteredBooks);
	return;
    // sending 404 when book is not found  
    res.status(404).send('Book not found');
});

/* Create - POST method */
app.post('/books/add', (req, res) => {
    //get the existing data
    const existData = getData()
    //get the new data from post request
    const bookData = req.body
	//check if the isbn exist or not       
    const findExist = existData.find( books => books.isbn === bookData.isbn )
    if (findExist) {
        return res.status(409).send({error: true, msg: 'isbn exist'})
    }
	// trim the leading zero
	bookData.isbn = bookData.isbn.replace(/^0+/, '');
    //append the data
    existData.push(bookData)
    //save the new data
    saveData(existData);
    res.send({success: true, msg: 'Data added successfully'})
})

/* Update - Post method */
app.post('/books/update/:isbn', (req, res) => {
    //get the isbn from url
    const isbn = req.params.isbn
    //get the update data
    const bookData = req.body
    //get the existing data
    const existData = getData()
    //check if the isbn exist or not       
    const findExist = existData.find( books => books.isbn === isbn )
    if (!findExist) {
        return res.status(409).send({error: true, msg: 'isbn not exist'})
    }
    //filter the bookData
    const updateData = existData.filter( books => books.isbn !== isbn )
    //push the updated data
    updateData.push(bookData)
    //finally save it
    saveData(updateData)
    res.send({success: true, msg: 'Data updated successfully'})
})

/* Delete - Delete method */
app.delete('/books/delete/:isbn', (req, res) => {
    const isbn = req.params.isbn
    //get the existing userdata
    const existData = getData()
    //filter the userdata to remove it
    const filterData = existData.filter( books => books.isbn !== isbn )
    if ( existData.length === filterData.length ) {
        return res.status(409).send({error: true, msg: 'isbn does not exist'})
    }
    //save the filtered data
    saveData(filterData)
    res.send({success: true, msg: 'Data removed successfully'})
})

/* util functions */
//read the data from json file
const saveData = (data) => {
    const stringifyData = JSON.stringify(data)
    fs.writeFileSync('books.json', stringifyData)
}

//get the data from json file
const getData = () => {
    const jsonData = fs.readFileSync('books.json')
    return JSON.parse(jsonData)    
}
/* util functions ends */

//configure the server port
app.listen(port, () => console.log("Server listening at http://%s:%s","localhost", port))
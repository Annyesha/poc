const express = require('express');
const app = express();
const port = 8000;

app.use(express.static('public'));
app.use('/js',express.static('public'));
app.use('/css',express.static('public'));
app.use('/html',express.static('public'));

app.get('/', (req, res) => {
    res.sendFile('./public/html/index.html',{root:__dirname});
})
app.get('/new-book', (req, res) => {
    res.sendFile('./public/html/new-book.html',{root:__dirname});
})
app.get('/edit-book', (req, res) => {
    res.sendFile('./public/html/edit-book.html',{root:__dirname});
})

//configure the server port
app.listen(port, () => console.log("Server listening at http://%s:%s","localhost", port))


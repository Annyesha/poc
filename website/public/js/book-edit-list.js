const setEditModal = (isbn) => {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", `http://localhost:3000/books/${isbn}`, false);
    xhttp.send();
    const book = JSON.parse(xhttp.responseText);
    const {
        title, 
        author, 
        description, 
    } = book;
    document.getElementById('isbn').value = isbn;
    document.getElementById('title').value = title;
    document.getElementById('author').value = author;
    document.getElementById('description').value = description;
}

const deleteBook = (isbn) => {
	var result = confirm("Are you sure to delete "+isbn+"?");
    if(result){
		const xhttp = new XMLHttpRequest();
		xhttp.open("DELETE", `http://localhost:3000/books/delete/${isbn}`, false);
		xhttp.send();
		location.reload();
    }
}

const loadBooks = () => {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://localhost:3000/books", false);
    xhttp.send();
    const books = JSON.parse(xhttp.responseText);
    for (let book of books) {
        const x = `
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">${book.title}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${book.isbn}</h6>
                        <div>Author: ${book.author}</div>
                        <div class="shortText">Description: ${book.description}</div>
                        <hr>
                        <button type="button" class="btn btn-danger" onClick="deleteBook(${book.isbn})">Delete</button>
                        <button types="button" class="btn btn-primary" data-toggle="modal" 
                        <button types="button" class="btn btn-primary" data-toggle="modal" 
                            data-target="#editBookModal" onClick="setEditModal(${book.isbn})">
                            View / Edit
                        </button>
                    </div>
                </div>
            </div>
		`
        document.getElementById('books').innerHTML = document.getElementById('books').innerHTML + x;
    }
}

loadBooks();